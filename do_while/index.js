// do{
//  hành động
// }
// while( điều kiện )
// var count = 0;

// while (count < 100) {
//   console.log("hello CyberSoft", count);
//   count++;
// }
// do {
//   console.log("yes");
// } while (false);

var count = 1000;

do {
  console.log("hello", count);
  count++;
} while (count < 100);

// do : cho user chọn trò , while ( khi user sai )

var number = 4;

var sum = 0;

do {
  sum += number;
  number--;
} while (number >= 1);
console.log(`  🚀 __ file: index.js:30 __ sum`, sum);
