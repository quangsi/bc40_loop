/**
 * for ( khởi tạo ;  điều kiện; bước nhảy)
 * {
 *  hành động
 * }
 *  vòng lặp, lần lặp
 */
var count = 0;

while (count < 10) {
  console.log("while_loop  hello CyberSoft ", count);
  count++;
}

for (var i = 0; i < 10; i = i + 2) {
  console.log("for_loop hello CyberSoft", i);
  console.log("for_loop hello CyberSoft", i);
}
// chạy 1
// ko chạy 0

// count có giá trị bao nhiu

var count = 0;
for (count; count <= 10; count++) {
  console.log("Do nothing", count);
}
console.log(count);
